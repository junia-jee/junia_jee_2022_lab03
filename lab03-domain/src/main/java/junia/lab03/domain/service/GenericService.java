package junia.lab03.domain.service;

import junia.lab03.domain.dao.GenericDAO;

import java.util.List;

//TODO create the 4 services which inherits this abstract class : CompanyService, ProjectService, CustomerService, ProjectService

public abstract class GenericService<T> {

    private final GenericDAO<T> internalDAO;

    protected GenericService(GenericDAO<T> internalDAO) {
        this.internalDAO = internalDAO;
    }

    public void deleteAll() {
        internalDAO.deleteAll();
    }

    public void save(final T objectToSave) {
        internalDAO.save(objectToSave);
    }

    public long countAll() {
        return internalDAO.count();
    }

    public List<T> findAll() {
        return internalDAO.findAll();
    }

}
