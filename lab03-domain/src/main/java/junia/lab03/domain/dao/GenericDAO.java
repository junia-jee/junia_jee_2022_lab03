package junia.lab03.domain.dao;


import java.util.List;

public interface GenericDAO<T> {

    void deleteAll();

    void save(T objectToSave);

    long count();

    List<T> findAll();
}
